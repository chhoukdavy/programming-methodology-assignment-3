package service;

import org.junit.Test;
import javax.ws.rs.DELETE;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by star on 11/17/16.
 */

public class DataServiceTest {
    @Test
    public void testGetPayerList(){
        DataService dataService = new DataService();
        assertThat(dataService.getPayerList(10).size(),is(10));
    }

}
