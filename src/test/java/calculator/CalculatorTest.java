package calculator;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by star on 11/18/16.
 */
public class CalculatorTest {
    @Test
    public void testGetTotalTax(){
        assertThat(Calculator.getTax(0.00), is(0.00));
        assertThat(Calculator.getTax(100.00), is(0.00));
        assertThat(Calculator.getTax(1000.00), is(0.00));
        assertThat(Calculator.getTax(150000.00), is(0.00));
        assertThat(Calculator.getTax(150001.00), is(0.10));

        assertThat(Calculator.getTax(500000.00), is(35000.00));
        assertThat(Calculator.getTax(500001.00), is(35000.20));
        assertThat(Calculator.getTax(753333.00), is(85666.60));
        assertThat(Calculator.getTax(925000.00), is(120000.00));

        assertThat(Calculator.getTax(1000000.00), is(135000.00));
        assertThat(Calculator.getTax(1000001.00), is(135000.30));
        assertThat(Calculator.getTax(2000000.00), is(435000.00));
        assertThat(Calculator.getTax(3007500.00), is(737250.0));

        assertThat(Calculator.getTax(4000000.00), is(1035000.00));
        assertThat(Calculator.getTax(4000001.00), is(1035000.37));
        assertThat(Calculator.getTax(5000000.00), is(1405000.00));
        assertThat(Calculator.getTax(10000000.00), is(3255000.00));
    }
}
