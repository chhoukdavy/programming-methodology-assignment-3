package service;

import com.sun.jersey.api.client.*;
import entity.Payer;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

import javax.swing.*;
import javax.ws.rs.core.MultivaluedMap;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by MorataX on 11/17/16.
 */

public class DataService {
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources");

    public List<Payer> getPayerList(int amount) {
        ClientConfig cfg = new DefaultClientConfig();
        cfg.getClasses().add(JacksonJsonProvider.class);
        Client client = Client.create(cfg);
        WebResource webResource = client.resource(resourceBundle.getString("apiServer") + "/api/payer/" + amount);
        List<Payer> output = null;
        try {

            ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);

            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
            }

            output = response.getEntity(new GenericType<List<Payer>>() {});

            return output;

        } catch (ClientHandlerException e) {
            JOptionPane.showMessageDialog(null, "Cannot Connect to the Internet! Please check your internet connection!", "Warning", JOptionPane.WARNING_MESSAGE);
        }
        return output;
    }
}

