import javax.swing.*;
import java.awt.*;

public class Main extends JFrame {
    public final int FHEIGHT = 340;
    public final int FWIDTH = 460;

    public static Main frame;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    frame = new Main();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public Main() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Progressive Tax Calculator");
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - FWIDTH) / 2);
        int y = (int) ((dimension.getHeight() - FHEIGHT) / 2);
        setBounds(x, y, FWIDTH, FHEIGHT);
        MainPanel mainPanel = new MainPanel();
        mainPanel.setBounds(0, 0, FWIDTH, FHEIGHT);
        add(mainPanel);
    }
}
