import calculator.Calculator;
import entity.Payer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.DataService;
import splash.Splash;
import table.TaxTable;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

/**
 * Created by star on 11/17/16.
 */

public class MainPanel extends JPanel {
    private JLabel lblNumPayer;
    private JTextField txfNumPayer;
    JButton btnLoad;

    DataService dataService;
    List<Payer> payerList;
    public static Splash splash;
    AnimationThread animationThread;

    int inputNum;

    Object[][] data;
    TaxTable taxTable;

    Logger logger = LoggerFactory.getLogger("taxLogger");
    Logger loggerSummary = LoggerFactory.getLogger("summeryLogger");

    public MainPanel() {
        setLayout(null);
        setVisible(true);

        lblNumPayer = new JLabel("Number of Tax Payer: ");
        lblNumPayer.setBounds(50, 75, 150, 25);
        add(lblNumPayer);

        txfNumPayer = new JTextField();
        txfNumPayer.setBounds(48, 110, 365, 35);
        txfNumPayer.setVisible(true);
        txfNumPayer.addActionListener(e -> {
            try {
                onTextInputAction(e);
            } catch (NumberFormatException numE) {
                JOptionPane.showMessageDialog(null, "Invalid Input! Try Again!", "Warning", JOptionPane.WARNING_MESSAGE);
            }
        });
        txfNumPayer.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {

            }

            public void keyPressed(KeyEvent e) {

            }

            public void keyReleased(KeyEvent e) {

            }
        });
        add(txfNumPayer);

        btnLoad = new JButton("Load");
        btnLoad.setBounds((460 - 100) / 2, 175, 100, 35);
        btnLoad.setVisible(true);
        btnLoad.addActionListener(e -> {
            try {
                onTextInputAction(e);
                onLoad(e);
            } catch (NumberFormatException numE) {
                JOptionPane.showMessageDialog(null, "Invalid Input! Try Again!", "Warning", JOptionPane.WARNING_MESSAGE);
            }
        });
        add(btnLoad);
    }

    private void onTextInputAction(ActionEvent e) throws NumberFormatException {
        String inputText = txfNumPayer.getText();
        if (txfNumPayer.getText().length() != 0) {
            inputNum = Integer.parseInt(inputText);
        }
    }

    private void onLoad(ActionEvent e) {
        //Start Animation
        animationThread = new AnimationThread();
        animationThread.start();

        //Load resource and when done stop animation
        LoadResource loadResourceThread = new LoadResource();
        loadResourceThread.start();
    }

//    private void printListPayer(List<Payer> listPayer) {
//        int counter = 1;
//        for (Payer payer : listPayer) {
//            System.out.print(counter + ". ");
//            System.out.println(payer.getName());
//            counter++;
//        }
//    }

    private Object[][] createTaxTableData(List<Payer> payerList) {
        Object[][] data = null;
        try {
            data = new Object[payerList.size()][4];
            NumberFormat formatter = new DecimalFormat("\u200E฿ #0000000.000");
            for (int i = 0; i < payerList.size(); i++) {
                data[i][0] = payerList.get(i).getFirstName();
                data[i][1] = payerList.get(i).getLastName();
                data[i][2] = formatter.format(payerList.get(i).getIncome());
                double tax = Calculator.getTax(payerList.get(i).getIncome());
                //Logging
                if (tax < 1000.0) {
                    logger.debug("{} {} - {}", payerList.get(i).getFirstName(), payerList.get(i).getLastName(), tax);
                } else if (tax <= 10000.0 && tax >= 1001.0) {
                    logger.info("{} {} - {}", payerList.get(i).getFirstName(), payerList.get(i).getLastName(), tax);
                } else if (tax > 10001.0) {
                    logger.warn("{} {} - {}", payerList.get(i).getFirstName(), payerList.get(i).getLastName(), tax);
                }
                data[i][3] = formatter.format(tax);
            }
        } catch (NullPointerException e) {
            System.out.println("Cannot connect to the internet! Please check your internet connection!");
        }
        return data;
    }

    private double getTotalTax(List<Payer> payerList) {
        double totalTax = 0.0;
        for (Payer payer : payerList) {
            totalTax += Calculator.getTax(payer.getIncome());
        }
        return totalTax;
    }

    private double getAverageTax(List<Payer> payerList) {
        long start = System.currentTimeMillis();
        double totalTax = getTotalTax(payerList);
        long end = System.currentTimeMillis();

        double average = getTotalTax(payerList) / payerList.size();
        loggerSummary.info("Total Tax: {} THB - Execution Time: {} MILLIS", totalTax, (end - start));
        return average;
    }

    private class LoadResource extends Thread {
        @Override
        public void run() {
            //Load Resource From Webservice
            dataService = new DataService();
            payerList = dataService.getPayerList(inputNum);

            //Stop Animation
            MainPanel.splash.setInvisible();
            Main.frame.setEnabled(true);
            Main.frame.setFocusable(true);

            //Show Payer Tax List
            data = createTaxTableData(payerList);
            if (data != null) {
                taxTable = new TaxTable(data, payerList.size(), getTotalTax(payerList), getAverageTax(payerList));
                taxTable.setVisible(true);
            }
        }
    }

    private class AnimationThread extends Thread {
        @Override
        public void run() {
            splash = new Splash();
            splash.setVisible();
            Main.frame.setEnabled(false);
            Main.frame.setFocusable(false);
        }
    }

}
