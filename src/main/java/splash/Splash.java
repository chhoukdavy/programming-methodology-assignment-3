package splash;

import javax.swing.*;
import java.awt.*;

/**
 * Created by star on 11/18/16.
 */

public class Splash{

    private JFrame frame;

    public Splash() {
        frame = new JFrame();
        frame.getContentPane().add(new LoadingImagePanel());
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setUndecorated(true);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.toFront();
        frame.setBackground(new Color(0, 0, 0, 0));
    }

    public void setVisible() {
        frame.setVisible(true);
    }
    public void setInvisible() {
        frame.setVisible(false);
    }
}
