package splash;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

/**
 * Created by star on 11/18/16.
 */

@SuppressWarnings("serial")
class LoadingImagePanel extends JPanel {
    //    BufferedImage img;
    private ClearImageIcon img;

    LoadingImagePanel() {
        setOpaque(false);
        setLayout(new GridBagLayout());
        img = new ClearImageIcon(LoadingImagePanel.class.getResource("/loading1.gif"));

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(img.getImage(), 0, 0, getWidth(), getHeight(), this);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(150, 150);
    }

    private class ClearImageIcon extends ImageIcon {
        ClearImageIcon(URL url) {
            super(url);
        }

        @Override
        public synchronized void paintIcon(Component c, Graphics g, int x, int y) {
            Graphics2D g2 = (Graphics2D) g.create();
            g2.setBackground(new Color(0, 0, 0, 0));
            g2.clearRect(0, 0, getIconWidth(), getIconHeight());
            super.paintIcon(c, g2, x, y);
        }
    }
}
