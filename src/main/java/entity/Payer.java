package entity;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by star on 11/17/16.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Payer {

    private String firstName;
    private String lastName;
    private double income;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getIncome() {
        return income;
    }

    public void setIncome(double income) {
        this.income = income;
    }

    public String getName(){
        return firstName + " " + lastName;
    }



}
