package table;

import javax.swing.*;
import java.awt.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by star on 11/18/16.
 */

public class TaxTable extends JFrame {

    JTable tblTax;
    JPanel panelTable;
    static String[] columnNames;
    Object[][] data;
    JScrollPane pane;

    JPanel panelSummary;
    JLabel lblSummary;
    JLabel lblTotalPayer;
    JLabel lblTotalTax;
    JLabel llbAverageTax;

    public TaxTable(Object[][] data, int totalPayer, double totalTax, double averageTax) {

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1125, 700);
        setTitle("Tax Table");
//        setExtendedState(MAXIMIZED_BOTH);
        setLayout(new BorderLayout(20, 20));

        panelTable = new JPanel();
        panelTable.setLayout(new FlowLayout());

        this.columnNames = new String[]{"First-Name", "Surname", "Income", "Tax"};
        this.data = data;

        tblTax = new JTable(this.data, this.columnNames);
        tblTax.setBounds(0, 0, 800, 700);
        tblTax.setVisible(true);
        tblTax.setPreferredScrollableViewportSize(new Dimension(800, 700));
        tblTax.setFillsViewportHeight(true);

        pane = new JScrollPane(tblTax);
        panelTable.add(pane);

        panelSummary = new JPanel();
        panelSummary.setVisible(true);
        panelSummary.setLayout(null);

        lblSummary = new JLabel();
        lblSummary.setText("Summary: ");
        lblSummary.setFont(new Font("Helvetica", Font.BOLD, 14));
        lblSummary.setBounds(25, 225, 300, 27);
        lblSummary.setVisible(true);
        panelSummary.add(lblSummary);

        lblTotalPayer = new JLabel();
        lblTotalPayer.setText("Total Payer: " + totalPayer + " payers");
        lblTotalPayer.setBounds(25, 250, 300, 27);
        lblTotalPayer.setVisible(true);
        panelSummary.add(lblTotalPayer);

        NumberFormat formatter = new DecimalFormat("\u200E฿ #0.000");

        lblTotalTax = new JLabel();
        lblTotalTax.setText("Total Tax:   " + formatter.format(totalTax));
        lblTotalTax.setBounds(25, 275, 300, 27);
        lblTotalTax.setVisible(true);
        panelSummary.add(lblTotalTax);

        llbAverageTax = new JLabel();
        llbAverageTax.setText("Average:     " + formatter.format(averageTax));
        llbAverageTax.setBounds(25, 300, 300, 27);
        llbAverageTax.setVisible(true);
        panelSummary.add(llbAverageTax);

        add(panelTable, BorderLayout.EAST);
        add(panelSummary, BorderLayout.CENTER);
    }

//    public static void main(String[] args) {
//        Object[][] objects = new Object[][]{
//                {"Davy", "Chhouk", 1000000, 500000},
//                {"Vicheka", "Nora", 1000000, 500000},
//                {"Gechhouy", "Lim", 1000000, 500000},
//                {"Laysothea", "Chhouk", 1000000, 500000},
//        };
//        TaxTable taxTable = new TaxTable(objects, 100, 10000.00, 1000.00);
//        taxTable.setVisible(true);
//    }

}
