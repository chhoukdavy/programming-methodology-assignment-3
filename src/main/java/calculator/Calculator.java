package calculator;

import java.util.Scanner;

/**
 * Created by star on 11/18/16.
 */

public class Calculator {

    //Tax Rate
    static final double RATE_TAX_00 = 0.00; // ฿0 - ฿150,000
    static final double RATE_TAX_10 = 0.10; // ฿150,001 - ฿500,000
    static final double RATE_TAX_20 = 0.20; // ฿500,001 – ฿1,000,000
    static final double RATE_TAX_30 = 0.30; // ฿1,000,001 – ฿4,000,000
    static final double RATE_TAX_37 = 0.37; // ฿4,000,000+

    //Income Bracket
    static final double BRACKET_0 = 150000;
    static final double BRACKET_10 = 500000;
    static final double BRACKET_20 = 1000000;
    static final double BRACKET_30 = 4000000;

    // max amount per bracket
    static final double MAX_TAX_10 = 35000;
    static final double MAX_TAX_20 = 100000;
    static final double MAX_TAX_30 = 900000;

    double income;
    static double x;
    static double totalTax = 0;
    Scanner keyboard = new Scanner(System.in);

    public static double getTax(double income) {
        if (income > BRACKET_30) {
            x = income - BRACKET_30;
            totalTax = MAX_TAX_10 + MAX_TAX_20 + MAX_TAX_30 + (x * RATE_TAX_37);

        } else if (income > BRACKET_20) {
            x = income - BRACKET_20;
            totalTax = MAX_TAX_10 + MAX_TAX_20 + (x * RATE_TAX_30);

        } else if (income > BRACKET_10) {
            x = income - BRACKET_10;
            totalTax = MAX_TAX_10 + (x * RATE_TAX_20);

        } else if (income > BRACKET_0) {
            x = income - BRACKET_0;
            totalTax = x * RATE_TAX_10;
        } else {
            totalTax = 0;
        }

        return totalTax;
    }
}

